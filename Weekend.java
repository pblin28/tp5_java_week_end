import java.util.ArrayList;
import java.util.List;

public class Weekend{
    
    private String dateDuWeekend;
    private List<Personne> participants;
    private List<Depense> lesDepenses;

    /** Permet d'initialiser un weekend avec une date, une liste de participants et une liste de dépenses.
     * 
     * @param date
     */
    public Weekend(String date){
        this.dateDuWeekend = date;
        List<Personne> participants = new ArrayList<>();
        List<Depense> lesDepenses = new ArrayList<>();
    }

    /** Permet d'ajouter un participant à la liste de personnes.
     * 
     * @param prenom
     * @param age
     */
    public void ajouteParticipant(String prenom, int age){
        this.participants.add(new Personne(prenom, age));
    }

    /** Permet d'ajouter une dépense à une liste de dépenses.
     * 
     * @param montant
     * @param produit
     * @param prenom
     */
    public void ajouteDepense(double montant, String produit, String prenom){
        for (Personne perso : participants){
            if(perso.getNom().equals(prenom)){
                Depense depense = new Depense(perso, montant, produit);
                lesDepenses.add(depense);
            }
        }
    }

    /** Permet de retourner le total des dépenses effectuées par une personne.
     * 
     * @param personne
     * @return (type) double
     */
    public double totalDepense(Personne personne){
        double total = 0.0;
        for (Depense depense : lesDepenses){
            if(depense.getPayeur().equals(personne)){
                total += depense.getMontant();
            }
        }
        return total;        
    }
                
    
    /** Permet de retourner le total des dépenses.
     * 
     * @return (type) double
     */
    public double totalDepense(){
        double total = 0.0;
        for (Depense depense : lesDepenses){
            total += depense.getMontant();
        }
        return total;
    }

    /** Permet de retourner le total de dépense avec le même produit acheté.
     * 
     * @param produit
     * @return (type) double
     */
    public double totalDepense(String produit){
        double total = 0.0;
        for (Depense depense : lesDepenses){
            if(depense.getProduit().equals(produit)){
                total = depense.getMontant();
            }
        }
        return total;
    }

    /** Permet de retourner l'avoir d'une personne.
     * 
     * @param personne
     * @return (type) double
     */
    public double avoirPersonne(Personne personne){
        return totalDepense()/participants.size()-this.totalDepense(personne);
    }


}
