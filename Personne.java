public class Personne{

    private String prenom;
    private int age;

    /** Permet d'identifier une personne avec un prénom et un âge.
     * 
     * @param prenom
     * @param age
     */
    public Personne(String prenom, int age) {
        this.prenom = prenom;
        this.age = age;
    }

    /** Permet de récuperer le prénom de la personne.
     * 
     * @return (type) String 
     */
    public String getNom(){
        return this.prenom;
    }

    /** Permet de retourner l'âge d'une personne.
     * 
     * @return (type) int
     */
    public int getAge(){
        return this.age;
    }

}