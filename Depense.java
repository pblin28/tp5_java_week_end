public class Depense{

    private double montant;
    private String produit;
    private Personne payeur;

    /** Permet d'identifier une dépense avec une personne, un montant et un produit.
     * 
     * @param personne
     * @param montant
     * @param produit
     */
    public Depense(Personne personne, double montant, String produit){
        this.payeur = personne;
        this.montant = montant;
        this.produit = produit;
    }
    
    /** Permet de retourner le nom du produit que la personne a achetée.
     * 
     * @return (type) String
     */
    public String getProduit(){
        return this.produit;
    }

    /** Permet de retourner le montant déboursé suite au produit acheté.
     * 
     * @return (type) double
     */
    public double getMontant(){
        return this.montant;
    }

    /** Permet de retourner la personne qui a payé.
     * 
     * @return (type) Personne
     */
    public Personne getPayeur(){
        return this.payeur;
    }


}